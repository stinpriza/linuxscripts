# emailCheck.sh

Low tech tool to cleanup mailing lists from unwanted emails before making a mass mailing. Performs various checks on a list of email adresses:

    Converts all addresses to lowercase
    Checks address' compliance against RC822
    Checks address' domain for known typos and corrects them (please help improve that list)
    Checks if email domain has MX records
    Checks if email user or domain is test / example / spam, rendering them ambiguous

Usage:

emailCheck.sh /path/to/email_list

Base script reads one email per line from input file. Script header contains instructions to read multicolumn CSV files. Warning: Using files comming from windows need prior conversion with dos2unix tool.
