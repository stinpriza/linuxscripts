#!/bin/sh

echo "###### lspci ######" >> data.log
sudo lspci >> data.log
echo "###### lspci -k ######" >> data.log
sudo lspci -k >> data.log
echo "###### lsmod ######" >> data.log
sudo lsmod >> data.log
echo "###### syslog ######" >> data.log
sudo tail -n 100 /var/log/syslog >> data.log
echo "###### dmesg ######" >> data.log
dmesg >> data.log
echo "###### xsession-errors ######" >> data.log
tail -n 100 ~/.xsession-errors >> data.log

mv data.log ~/Desktop/data.log-`date +"%Y%m%d_%H%M%S"`
echo "log entries/info written to ~/Desktop/data.log-`date +"%Y%m%d_%H%M%S"`"

exit 0
