#! /bin/sh
# script for checking if timeshift has as default partition root and change it to home
# if root partition is small, timeshift can flood it


if [[ ! -z $(cat /etc/fstab | grep UUID | grep home | awk '{print $1}') ]]; then

    echo "there is a separate home partition, so we change timeshift default partition"

#just in case, make a backup of the old config, moving the file
mv /etc/timeshift.json /etc/timeshift.json.bkp

#find home partition UUID
myuuid=$(cat /etc/fstab | grep UUID | grep home | awk '{print $1}')

#create the new config
echo { > /etc/timeshift.json
echo  '"backup_device_uuid" : "$myuuid",' > /etc/timeshift.json
grep -v '{' /etc/timeshift.json.bkp | grep backup >> /etc/timeshift.json


else
    echo "no separate home partition, so nothing to be done"
fi
