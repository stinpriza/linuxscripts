## INTRO ##

small script for automation of merge pdfs with the syntax: a-1.pdf a-2.pdf ... a-100.pdf to a single file named a.pdf. It can be used for different group of files
also for b-1.pdf, b2.pdf -> b.pdf etc.


## REQUIREMENTS ##

pdftk package, you can install it through your pakage manager


## USAGE ##

1. **downoald the file**

`wget https://framagit.org/stinpriza/batch-merge-pdf-files/raw/master/batch_merge_pdf.sh`
 
2. **give proper permissions**

`chmod 755 batch_merge_pdf.sh`

3. **edit and add the names of all the file-groups you need.**

 `nano batch_merge_pdf.sh`

4. **run it with** 

`./batch_merge_pdf.sh`