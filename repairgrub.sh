#! /bin/sh
# Script to automatically repair broken grub
# needs to run from a live cd/usb
# needs root partition of valid gnu/linux installation as a parameter
# source: http://howtoubuntu.org/how-to-repair-restore-reinstall-grub-2-with-a-ubuntu-live-cd

# color combinations for use in echo
RED='\033[0;31m'
GREEN='\033[0;32m'
BLUE='\033[0;34m'
PURPLE='\033[0;35m'
CYAN='\033[0;36m'
GREY='\033[0;37m'
NC='\033[0m' # No Color

#checking if parttion is valid
PARTITION="$1"

echo "$PARTITION" | grep -q /

if test $? -ne 0; then
        PARTITION="/dev/${PARTITION}"
fi

if test ! -b "$PARTITION"; then
        echo "${RED}Specified partition $PARTITION is not a block device${NC}" 1>&2
        exit 2
fi

#getting the disk out of the partition
DISK=`echo "$PARTITION" | sed 's/[0-9]*//g'`

echo "################################################################################################"
echo "colors legend: ${GREY}comment, ${RED}error, ${GREEN}success, ${PURPLE}result, ${CYAN}prompt to act"
echo "${GREY}working disk is ${PURPLE}$DISK${NC} and partition is ${PURPLE}$PARTITION${NC}"
echo "################################################################################################"

# mounting stuff
echo "${GREY}mounting the given root partition${NC}"
echo "${GREY}sudo mount /dev/$1 /mnt${NC}"
sudo mount /dev/$1 /mnt
echo "${GREEN}done${NC}"
if test $? -ne 0; then
        echo "${RED}Command such and such failed${NC}" 1>&2
        exit 1
fi

echo "${GREY}mounting needed system folders${NC}"
echo "${GREY}sudo mount --bind /dev /mnt/dev && sudo mount --bind /dev/pts /mnt/dev/pts && sudo mount --bind /proc /mnt/proc && sudo mount --bind /sys /mnt/sys${NC}"
sudo mount --bind /dev /mnt/dev && sudo mount --bind /dev/pts /mnt/dev/pts && sudo mount --bind /proc /mnt/proc && sudo mount --bind /sys /mnt/sys
if test $? -ne 0; then
        echo "${RED}Command mount folders failed${NC}" 1>&2
        exit 1
fi
echo "${GREEN}done${NC}"

# chroot to mounted filesystem
echo "${GREY}chroot to the installation${NC}"
echo "${GREY}sudo chroot /mnt${NC}"
echo "######################################################################################"
echo "${CYAN}After chroot, run the following commands"
echo "${CYAN}grub-install $DISK && grub-install --recheck $DISK && update-grub  && exit${NC}"
echo "######################################################################################${CYAN}"
sudo chroot /mnt
if test $? -ne 0; then
        echo "${RED}Command chroot failed${NC}" 1>&2
        exit 1
fi
echo "${GREEN}done${NC}"

echo "${CYAN}Shut down and turn your computer back on, and you will be met with the default Grub2 screen.${NC}"

exit 0
